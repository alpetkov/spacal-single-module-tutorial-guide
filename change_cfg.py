import pickle 
import shutil


def load_obj(name):
	with open(name + '.pkl', 'rb') as f:
		return pickle.load(f)

d = load_obj("strange_shape_pickle")


file_path = "spacal_W_gfag_pitch1.67_12.12x12.12cm.cfg"
new_file = "new_config.cfg"

#shutil.copy(file_path, new_file)

lines=[]
with open(file_path, 'r', encoding='utf-8') as file:
	for line in file:
		for key in d.keys():
			if line.startswith(key):
				lines.append(line)

with open(file_path, 'r', encoding='utf-8') as infile:
	with open(new_file, 'w', encoding='utf-8') as outfile:
		outfile.truncate()
		for line in infile:
			if line in lines: 
				for key in d.keys():
					if line.startswith(key):
						index = line.index('=') + 2
						temp = line[index:]
						new_line = line.replace(temp,d[key])
						outfile.write(new_line+'\n')
			else: 
				outfile.write(line)




