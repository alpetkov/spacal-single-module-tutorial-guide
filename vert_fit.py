import ROOT
import numpy as np
from ROOT import *
from ROOT import RDataFrame

path = '/home/alpetkov/RadiationDamage/55_95/4/outs/out*.root'
folder = 'photons'

data = RDataFrame(folder, path)

bins = np.arange(0, 95.0, 1.0)

data_rescaled = data.Define("vertZ_res", "75 + (-1)*vertZ")
h_vertZ = data_rescaled.Histo1D(ROOT.RDF.TH1DModel("h_vertZ", "h_vertZ", len(bins)-1, bins), 'vertZ_res').GetPtr()

f = TF1("f", "[0]*TMath::Exp((-1)*x/[1])", 10.0, 85.0)
f.SetParameter(0,17200.0)
f.SetParameter(1, 272.0)

c = TCanvas()
h_vertZ.Fit(f, "Vr")
h_vertZ.Draw()
c.SaveAs("fit.pdf")



	
	
