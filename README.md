# Performing one SPACAL single module simulation 

Hello! This (very) informal document will cover all the basic steps I took in order to run the single module simulation. I will assume that you already have the simulation downloaded locally and on AFS. 


## Files that you need to start 

Make a directory where you would want to store the files for the simulation that you are about to run.

### Configuration files

Whichever geometrical or physical change you want to make on the module, you do it in the base configuration file. You can find different module configurations in **/path/to/simulation/configurations/FullEcalSimulations**, depending on which Run you choose. For example, base configuration that I was working with was from Run5 folder, **spacal_W_gfag_pitch1.16_12.12x12cm.cfg**. You will also need signal configuration file which can be found in the same folder, I was using **SignalConfigFile_HPKR7600U-20_FL1_CFD02.cfg**. 

To my understanding, if you want to make a change that is not related to the geometry of the module (here I exclude switching between different modules), you can do that by changing manually lines in the .cfg file. For instance, this is what I was doing when I wanted to change the bulk absorption length scale factor (at the end of the file). One could write a script to make this more automatic, but it's not complicated to do it manually so I just kept it as it is.

However, geometrical changes are done using a script **make_strings.py**. Normally, what this code was doing before I altered it a bit was just printing out a big text that you then manually need to replace in the .cfg file. You can find more details about this in the Tutorial. I found this a bit annoying, so I made a script to do it for us, named **change_cfg.py**, and altered make_strings.py to automatically produce a pickle file **strange_shape_pickle.pkl** which contains a dictionary filled with what was previously printed out by make_strings.py. This means that you can do whatever geometrical change you need in make_strings.py, run it, and then just run change_cgf.py which will produce the new configuration file called **new_config.cfg**.


### Other files 
Apart from the base and signal configuration files (this one you don't change), you need to copy the following into your directory
- gps_e_3+3.mac (or equivalent to this, might vary due to which particles you're shooting);
- pulseVisualization.C;
- propagateHybrid;
- vis.mac.

I believe that a detailed description of these is given in the Tutorial.

### First steps 

This part is pretty much the same as explained in the Tutorial. You can visualize your module using **$SPACAL/build/FibresCalo new_config.cfg**, where SPACAL is just the path to where you stored the simulation. Once we have the base configuration that we want, we can shoot and visualize some particles using **$SPACAL/build/FibresCalo new\_config.cfg out gps\_e\_3+3.mac**. This will create file out.root that is nice to look at some stuff as suggested in the Tutorial but, in the grand scheme of things, you don't need it to extract data for analysis.

## Optical calibration

### Producing out*.root files

This step usually needs to be done on LXPLUS. So, what you should do is create a folder on AFS where you can copy everything that you already have in the folder on your local machine. You should also make a folder on EOS where you'd like the output of optical calibration to be stored. You will additionally need file **optical_calibration.sh**. Inside this file, you need to change SIMPATH to where you stored the simulation file on AFS. Then, CONFIG is a path to where you uploaded the configuration that you're using in this run of the simulation, so if you just copied everything from the local folder to AFS folder, it's the location of new_config.cfg. Finally, BASEFOLDEROUT is the path to your EOS output folder. Once you did all of this, you need to source it. That will produce a file called **runAll_jobs.sh**, which you then just run and it will automatically submit jobs to condor. This will run for a few hours (if you're lucky), and at the end you will get a lot of out files in your EOS folder. 


### Merging out*.root files 

Now we want to merge all of these out files so that we can more easily deal with them. Copy **merge_optical.sh** to your AFS folder and run it with the following **./merge.sh /path/to/your/eos/folder time nbins**. Usually I was using time = 10 (in ns) and 200 as the number of bins. This will create a folder** MergeOptical_time_nbins**, which you should enter and submit the jobs by typing **condor_submit jobs.sub**. When merge is done, a file named **calibration_time_nbins.data** will be created in the EOS folder.

**VERY IMPORTANT**: Before merging these files, you want to check that none of them are corrupted in any way. You can do this by going to the EOS directory and writing the following command: **find -name "out*.root" -type 'f' -size 5k**, which will first check if there are no empty files. However, a corrupted file might be just slightly smaller than a healthy file, so you can check that, for instance, out0.root is healthy and then look at its size. Then execute the previous command but instead of 5k write something slightly smaller than the size of out0.root. In case you find some empty/corrupted files, just remove them (this is easy to do by simply adding -delete at the end of the command I gave above).


## Time and energy information 

### Getting the files

To extract time and energy information from the merged out files that you produced, you just need **prepare.sh** file. Inside of it, you need to put correct paths for the platform you're using. So, if you want to do this on LXPLUS, you will change SIMPATH to the location of your simulation on AFS, LISTCALIBRATIONS to where calibration_time_nbins.data is located and FLUX you can leave as it is because it's not relevant for one module study. Additionally, when executing prepare.sh you should add as arguments lxplus (platform where you want to run it) and pgun flag. Use of this is explained in more detail in the Tutorial. After this you will get a lot of output files separated into energy folders. You should check that there are no empty files in any of these folders in a similar way as we checked it for merging. Before creating the plots, you should copy contents of the folder Analysis, located at **/path/to/simulation/1.SingleModuleStudy/Analysis**. Then, simply run **go.sh**.

### Analyzing the files 
