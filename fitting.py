import ROOT
from ROOT import TF1, TMath
from ROOT import *

file = ROOT.TFile.Open('/home/alpetkov/RadiationDamage/75_75/0.15/energy.root')

graph = file.Get('graph_energy')
fD = TF1("fD", "TMath::Sqrt(TMath::Power([0]/TMath::Sqrt(x),2)+TMath::Power([1],2))", 0, 100)
fD.SetParameter(0,0.10)
fD.SetParameter(1,0.01)



c = ROOT.TCanvas()
graph.Fit(fD,"Vr")
graph.Draw()
c.SaveAs("fit_energy.pdf")


